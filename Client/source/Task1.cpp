#include "Task1.h"

Task1::Task1() : Request("Task1")
{
	this->content.push_back(boost::property_tree::ptree::value_type("ProcentRataCriminalitate", "Procent rata criminalitate"));
	this->content.push_back(boost::property_tree::ptree::value_type("An1", "1998"));
	this->content.push_back(boost::property_tree::ptree::value_type("An2", "2004"));
}
