#include "Task2.h"

Task2::Task2() : Request("Task2")
{
	this->content.push_back(boost::property_tree::ptree::value_type("Furturi", "Cate furturi au fost facute?"));
	this->content.push_back(boost::property_tree::ptree::value_type("An1", "1998"));
	this->content.push_back(boost::property_tree::ptree::value_type("An2", "2004"));
}
