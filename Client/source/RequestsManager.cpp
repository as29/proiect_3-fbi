#include "RequestsManager.h"

#include "HandshakeRequest.h"
#include "SumRequest.h"
#include "WordCountRequest.h"
#include "ProjectRequest.h"
#include "Task1.h"
#include "Task2.h"
#include "Task3.h"
#include "Task4.h"
#include "Task5.h"

RequestsManager::RequestsManager()
{
	this->requests.emplace("Handshake", std::make_shared<HandshakeRequest>());
	this->requests.emplace("Sum", std::make_shared<SumRequest>());
	this->requests.emplace("WordCounter", std::make_shared<WordCountRequest>());
	this->requests.emplace("Project", std::make_shared<ProjectRequest>());
	this->requests.emplace("Task1", std::make_shared<Task1>());
	this->requests.emplace("Task2", std::make_shared<Task2>());
	this->requests.emplace("Task3", std::make_shared<Task3>());
	this->requests.emplace("Task4", std::make_shared<Task4>());
	this->requests.emplace("Task5", std::make_shared<Task5>());
}

std::map<std::string, std::shared_ptr<Framework::Request>> RequestsManager::getMap() const
{
	return this->requests;
}
