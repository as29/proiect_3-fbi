#include "ProjectRequest.h"

ProjectRequest::ProjectRequest() : Request("Project")
{
	this->content.push_back(boost::property_tree::ptree::value_type("test1", "1"));
	this->content.push_back(boost::property_tree::ptree::value_type("test2", "2"));
}
