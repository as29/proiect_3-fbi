#include <iostream>
#include <memory>

#include "Session.h"

int main(int argc, char** argv)
{
	const auto host = "192.168.0.105";
	const auto port = "2751";

	// The io_context is required for all I/O
	boost::asio::io_context ioContext;

	RequestsManager requestsManager;

	//while(true) {

		std::cout << "1.Handshake"<<std::endl;
		std::cout << "2.Sum" << std::endl;
		std::cout << "3.WordCounter" << std::endl;
		std::cout << "4.Project" << std::endl;
		std::cout << "5.Task1" << std::endl;
		std::cout << "6.Task2" << std::endl;
		std::cout << "7.Task3" << std::endl;
		std::cout << "8.Task4" << std::endl;
		std::cout << "9.Task5" << std::endl;
		std::cout << "10.Exit" << std::endl;
		


		int option;
		std::cin >> option;

		if(option!=10){
			switch (option) {
			case 1:
			{
				auto handshake = requestsManager.getMap().at("Handshake")->getContentAsString();
				std::make_shared<Session>(ioContext)->run(host, port, handshake);
				ioContext.run();
				break;
			}
			case 2:
			{
				auto sum = requestsManager.getMap().at("Sum")->getContentAsString();
				std::make_shared<Session>(ioContext)->run(host, port, sum);
				ioContext.run();
				break;
			}
			case 3:
			{
				auto wordcounter = requestsManager.getMap().at("WordCounter")->getContentAsString();
				std::make_shared<Session>(ioContext)->run(host, port, wordcounter);
				ioContext.run();
				break;
			}
			case 4:
			{
				auto project = requestsManager.getMap().at("Project")->getContentAsString();
				std::make_shared<Session>(ioContext)->run(host, port, project);
				ioContext.run();
				break;
			}
			case 5:
			{
				auto task1 = requestsManager.getMap().at("Task1")->getContentAsString();
				std::make_shared<Session>(ioContext)->run(host, port, task1);
				ioContext.run();
				break;
			}
			case 6:
			{
				auto task2 = requestsManager.getMap().at("Task2")->getContentAsString();
				std::make_shared<Session>(ioContext)->run(host, port, task2);
				ioContext.run();
				break;
			}
			case 7:
			{
				auto task3 = requestsManager.getMap().at("Task3")->getContentAsString();
				std::make_shared<Session>(ioContext)->run(host, port, task3);
				ioContext.run();
				break;
			}
			case 8:
			{
				auto task4 = requestsManager.getMap().at("Task4")->getContentAsString();
				std::make_shared<Session>(ioContext)->run(host, port, task4);
				ioContext.run();
				break;
			}
			case 9:
			{
				auto task5 = requestsManager.getMap().at("Task5")->getContentAsString();
				std::make_shared<Session>(ioContext)->run(host, port, task5);
				ioContext.run();
				break;
			}
			case 10:
			{
				system("pause");
				return EXIT_SUCCESS;
			}

			}
		//}
	}

	//auto handshake = requestsManager.getMap().at("Handshake")->getContentAsString();
	//auto sum = requestsManager.getMap().at("Sum")->getContentAsString();
	//auto wordcounter = requestsManager.getMap().at("WordCounter")->getContentAsString();
	//auto project = requestsManager.getMap().at("Project")->getContentAsString();
	//auto task1 = requestsManager.getMap().at("Task1")->getContentAsString();
	//auto task2 = requestsManager.getMap().at("Task2")->getContentAsString();
	//auto task3 = requestsManager.getMap().at("Task3")->getContentAsString();
	//auto task4 = requestsManager.getMap().at("Task4")->getContentAsString();
	//auto task5 = requestsManager.getMap().at("Task5")->getContentAsString();

	// Launch the asynchronous operation
	//std::make_shared<Session>(ioContext)->run(host, port, handshake);
	//std::make_shared<Session>(ioContext)->run(host, port, sum);
	//std::make_shared<Session>(ioContext)->run(host, port, wordcounter);
	//std::make_shared<Session>(ioContext)->run(host, port, project);
	//std::make_shared<Session>(ioContext)->run(host, port, task1);
	//std::make_shared<Session>(ioContext)->run(host, port, task2);
	//std::make_shared<Session>(ioContext)->run(host, port, task3);
	//std::make_shared<Session>(ioContext)->run(host, port, task4);
	//std::make_shared<Session>(ioContext)->run(host, port, task5);


	// Run the I/O service. The call will return when
	// the socket is closed.
	ioContext.run();
	system("pause");

	return EXIT_SUCCESS;
}