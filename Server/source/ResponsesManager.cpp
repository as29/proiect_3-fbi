#include "ResponsesManager.h"

#include "HandshakeResponse.h"
#include "SumResponse.h"
#include "WordCountResponse.h"
#include "ProjectResponse.h"
#include "Task1.h"
#include "Task2.h"
#include "Task3.h"
#include "Task4.h"
#include "Task5.h"


ResponsesManager::ResponsesManager()
{
	this->Responses.emplace("Handshake", std::make_shared<HandshakeResponse>());
	this->Responses.emplace("Sum", std::make_shared<SumResponse>());
	this->Responses.emplace("WordCounter", std::make_shared<WordCountResponse>());
	this->Responses.emplace("Project", std::make_shared<ProjectResponse>());
	this->Responses.emplace("Task1", std::make_shared<Task1>());
	this->Responses.emplace("Task2", std::make_shared<Task2>());
	this->Responses.emplace("Task3", std::make_shared<Task3>());
	this->Responses.emplace("Task4", std::make_shared<Task4>());
	this->Responses.emplace("Task5", std::make_shared<Task5>());
}

std::map<std::string, std::shared_ptr<Framework::Response>> ResponsesManager::getMap() const
{
	return this->Responses;
}
