#include "RowTable1.h"

RowTable1::RowTable1()
{
}

RowTable1::RowTable1(int year, double population, double violentCrime, float violentCrime_r, double murderNM, float murderNM_r, double rape, float rape_r, double robbery, float robbery_r, double aggravatedAssault, float aggravatedAssault_r, double propertyCrime, float propertyCrime_r, double burglary, float burglary_r, double larencyTeft, float larencyTeft_r, double motorVehicleTeft, float motorVehicleTeft_r)
{
		this->year = year;
		this->population = population;
		this->violentCrime = violentCrime;
		this->violentCrime_r = violentCrime_r;
		this->murderNM = murderNM;
		this->murderNM_r = murderNM_r;
		this->rape = rape;
		this->rape_r = rape_r;
		this->robbery = robbery;
		this->robbery_r = robbery_r;
		this->aggravatedAssault = aggravatedAssault;
		this->aggravatedAssault_r = aggravatedAssault_r;
		this->propertyCrime = propertyCrime;
		this->propertyCrime_r = propertyCrime_r;
		this->burglary = burglary;
		this->burglary_r = burglary_r;
		this->larencyTeft = larencyTeft;
		this->larencyTeft_r = larencyTeft_r;
		this->motorVehicleTeft = motorVehicleTeft;
		this->motorVehicleTeft_r = motorVehicleTeft_r;
}

RowTable1::~RowTable1()
{
}

int RowTable1::getYear()
{
	return year;
}

double RowTable1::getPopulation()
{
	return population;
}

double RowTable1::getViolentCrime()
{
	return violentCrime;
}

float RowTable1::getViolentCrime_r()
{
	return violentCrime;
}

double RowTable1::getMurderNM()
{
	return murderNM;
}

float RowTable1::getMurderNM_r()
{
	return murderNM_r;
}

double RowTable1::getRape()
{
	return rape;
}

float RowTable1::getRape_r()
{
	return rape_r;
}

double RowTable1::getRobbery()
{
	return robbery;
}

float RowTable1::getRobbery_r()
{
	return robbery_r;
}

double RowTable1::getAggravatedAssault()
{
	return aggravatedAssault;
}

float RowTable1::getAggravatedAssault_r()
{
	return aggravatedAssault_r;
}

double RowTable1::getPropertyCrime()
{
	return propertyCrime;
}

float RowTable1::getPropertyCrime_r()
{
	return propertyCrime_r;
}

double RowTable1::getBurglary()
{
	return burglary;
}

float RowTable1::getBurglary_r()
{
	return burglary_r;
}

double RowTable1::getLarencyTeft()
{
	return larencyTeft;
}

float RowTable1::getLarencyTeft_r()
{
	return larencyTeft_r;
}

double RowTable1::getMotorVehicleTeft()
{
	return motorVehicleTeft;
}

float RowTable1::getMotorVehicleTeft_r()
{
	return motorVehicleTeft_r;
}

void RowTable1::setYear(int year)
{
	this->year = year;
}

void RowTable1::setPopulation(double population)
{
	this->population = population;
}

void RowTable1::setViolentCrime(double violentCrime)
{
	this->violentCrime = violentCrime;
}

void RowTable1::setViolentCrime_r(float violentCrime_r)
{
	this->violentCrime_r = violentCrime_r;
}

void RowTable1::setMurderNM(double murderNM)
{
	this->murderNM = murderNM;
}

void RowTable1::setMurderNM_r(float murderNM_r)
{
	this->murderNM_r = murderNM_r;
}

void RowTable1::setRape(double rape)
{
	this->rape = rape;
}

void RowTable1::setRape_r(float rape_r)
{
	this->rape_r = rape_r;
}

void RowTable1::setRobbery(double robbery)
{
	this->robbery = robbery;
}

void RowTable1::setRobbery_r(float robbery_r)
{
	this->robbery_r = robbery_r;
}

void RowTable1::setAggravatedAssault(double aggravatedAssault)
{
	this->aggravatedAssault = aggravatedAssault;
}

void RowTable1::setAggravatedAssault_r(float aggravatedAssault_r)
{
	this->aggravatedAssault_r = aggravatedAssault_r;
}

void RowTable1::setPropertyCrime(double propertyCrime)
{
	this->propertyCrime = propertyCrime;
}

void RowTable1::setPropertyCrime_r(float propertyCrime_r)
{
	this->propertyCrime_r = propertyCrime_r;
}

void RowTable1::setBurglary(double burglary)
{
	this->burglary = burglary;
}

void RowTable1::setBurglary_r(float burglary_r)
{
	this->burglary_r = burglary_r;
}

void RowTable1::setLarencyTeft(double larencyTeft)
{
	this->larencyTeft = larencyTeft;
}

void RowTable1::setLarencyTeft_r(float larencyTeft_r)
{
	this->larencyTeft_r = larencyTeft_r;
}

void RowTable1::setMotorVehicleTeft(double motorVehicleTeft)
{
	this->motorVehicleTeft = motorVehicleTeft;
}

void RowTable1::setMotorVehicleTeft_r(float motorVehicleTeft_r)
{
	this->motorVehicleTeft_r = motorVehicleTeft_r;
}


