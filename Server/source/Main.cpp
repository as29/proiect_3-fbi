#pragma once
#include <iostream>
#include <thread>
#include "Listener.h"
#include <fstream>
#include <string>
#include <stdlib.h>
#include <vector>
#include "Table39.h"
#include"util.h"
#include"Table1.h"
int main()
{
	util t;
	const auto address = boost::asio::ip::make_address("192.168.0.105");
	const unsigned short port = 2751;
	const auto threadsCount = 10;

	// The io_context is required for all I/O
	//fisirele
	boost::asio::io_context ioContext{ threadsCount };
	std::ifstream t1("..\\..\\Server\\asset\\Table1.csv");
	std::ifstream t39("..\\..\\Server\\asset\\Table39.csv");
	std::ifstream t40("..\\..\\Server\\asset\\Table40.csv");

	
	std::vector<Table1*>table1;
	std::vector<Table39*> table39;
	std::vector<Table39*> table40;

	//citim datele din fisiere
	table1 = t.readTable1(t1);
	table39 = t.readTable39(t39);
	table40 = t.readTable39(t40);

	std::cout << table1.size() << std::endl;
	std::cout << table39.size() << std::endl;
	std::cout << table40.size() << std::endl;
	

	// ceva calcule
	// suma pe "Total all ages"
	double totalAgesSum = 0;
	for (Table39 *t : table39) {
		totalAgesSum += t->getTotalAllAges();
	}

	//std::cout << "TotalAges count: " << totalAgesSum << std::endl;



	// Create and launch a listening port
	std::make_shared<Listener>(ioContext, boost::asio::ip::tcp::endpoint{ address, port })->run();

	// Run the I/O service on the requested number of threads
	std::vector<std::thread> threads;

	threads.reserve(threadsCount - 1);
	for (auto index = 0; index < threadsCount - 1; ++index)
	{
		threads.emplace_back([&ioContext] { ioContext.run(); });
	}

	ioContext.run();



	return EXIT_SUCCESS;
}
