#include <stdlib.h>

#include "Task4.h"

Task4::Task4() : Response("Task4")
{

}

std::string Task4::interpretPacket(const boost::property_tree::ptree& packet)
{
	auto drug = atoi(packet.get<std::string>("Drug").c_str());

	//auto drug = 0;
	//float furturi = Util.fct1()

	this->content.push_back(boost::property_tree::ptree::value_type("File", "drug.txt"));
	this->content.push_back(boost::property_tree::ptree::value_type("Result", std::to_string(drug)));

	return this->getContentAsString();
}


