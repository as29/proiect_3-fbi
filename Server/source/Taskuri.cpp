#include "Taskuri.h"
#include<array>
#include"util.h"
double Taskuri::sumaFurturi(int an1, int an2)
{
	double total=0;
	for (auto linie : tabel1)
	{
		if (linie->getYear() >= an1 && linie->getYear() <= an2)
		{
			total += linie->getBurglary() + linie->getRobbery();
		}
	}
	return total;
}

std::vector<std::string> Taskuri::topInfractiuni()
{
	Table1 sum;
	std::array<std::string, 9> campuri = { "violentCrime","murderNM","rape","robbery","aggravatedAssault","propertyCrime","burglary","larencyTeft","motorVehicleTeft" };

	std::array<std::pair<std::string,double>,9> suma;
	for (int index = 0; index < 9; index++)
	{
		suma[index].first = campuri[index];
		suma[index].second = 0;
	}

	for (auto& linie : tabel1)
	{
		suma[0].second += linie->violentCrime;
		suma[1].second += linie->murderNM;
		suma[2].second += linie->rape;
		suma[3].second += linie->robbery;
		suma[4].second += linie->aggravatedAssault;
		suma[5].second += linie->propertyCrime;
		suma[6].second += linie->burglary;
		suma[7].second += linie->larencyTeft;
		suma[8].second += linie->motorVehicleTeft;
	}
	std::sort(suma.begin(), suma.end(), [](auto &left, auto &right) {
		return left.second > right.second;
		});
	std::vector<std::string> top;
	for (int index = 0; index < 3; index++)
	{
		top.push_back(suma[index].first);
	}
	return top;
}

float Taskuri::rataOmor(int an1, int an2)
{
	float primul = 0;
	float alDoilea=0;
	for (auto& linie : tabel1)
	{
		if (linie->year == an1)
			primul = linie->getMurderNM_r();
		if (linie->year == an2)
			alDoilea = linie->getMurderNM_r();
	}
	return alDoilea - primul;
}

std::vector<std::string> Taskuri::infractiuniVarsta()
{
	std::array<std::string, 21> campuri = { "<10","10-12","13-14","15","16","17","18","20","21","22","23","24","25-29","30-34","35-39","40-44","45-49","50-54","55-59","60-64","65" };
	std::array<std::pair<std::string, float>, 21> totalb;
	std::array<std::pair<std::string, float>, 21> totalf;
	for (int index = 0; index < 21; index++)
	{
		totalb[index].first = campuri[index];
		totalf[index].first = campuri[index];
	}
	totalb[0].second = tabel39[1]->under10;
	totalb[1].second = tabel39[1]->age10_12;
	totalb[2].second = tabel39[1]->age13_14;
	totalb[3].second = tabel39[1]->age15;
	totalb[4].second = tabel39[1]->age16;
	totalb[5].second = tabel39[1]->age17;
	totalb[6].second = tabel39[1]->age18;
	totalb[7].second = tabel39[1]->age20;
	totalb[8].second = tabel39[1]->age21;
	totalb[9].second = tabel39[1]->age22;
	totalb[10].second = tabel39[1]->age23;
	totalb[11].second = tabel39[1]->age24;
	totalb[12].second = tabel39[1]->age25_29;
	totalb[13].second = tabel39[1]->age30_34;
	totalb[14].second = tabel39[1]->age35_39;
	totalb[16].second = tabel39[1]->age40_44;
	totalb[17].second = tabel39[1]->age50_54;
	totalb[18].second = tabel39[1]->age55_59;
	totalb[19].second = tabel39[1]->age60_64;
	totalb[20].second = tabel39[1]->ageOver65;


	totalf[0].second = tabel39[1]->under10;
	totalf[1].second = tabel39[1]->age10_12;
	totalf[2].second = tabel39[1]->age13_14;
	totalf[3].second = tabel39[1]->age15;
	totalf[4].second = tabel39[1]->age16;
	totalf[5].second = tabel39[1]->age17;
	totalf[6].second = tabel39[1]->age18;
	totalf[7].second = tabel39[1]->age20;
	totalf[8].second = tabel39[1]->age21;
	totalf[9].second = tabel39[1]->age22;
	totalf[10].second = tabel39[1]->age23;
	totalf[11].second = tabel39[1]->age24;
	totalf[12].second = tabel39[1]->age25_29;
	totalf[13].second = tabel39[1]->age30_34;
	totalf[14].second = tabel39[1]->age35_39;
	totalf[16].second = tabel39[1]->age40_44;
	totalf[17].second = tabel39[1]->age50_54;
	totalf[18].second = tabel39[1]->age55_59;
	totalf[19].second = tabel39[1]->age60_64;
	totalf[20].second = tabel39[1]->ageOver65;

	std::sort(totalb.begin(), totalb.end(), [](auto &left, auto &right) {
		return left.second > right.second;
		});
	std::sort(totalf.begin(), totalf.end(), [](auto &left, auto &right) {
		return left.second > right.second;
		});
	std::vector<std::string> raspuns;
	raspuns.push_back(totalb[0].first);
	raspuns.push_back(totalf[0].first);
	return raspuns;
}

Taskuri::Taskuri()
{
	util t;

	std::ifstream t1("..\\..\\Server\\asset\\Table1.csv");
	std::ifstream t39("..\\..\\Server\\asset\\Table39.csv");
	std::ifstream t40("..\\..\\Server\\asset\\Table40.csv");


	std::vector<Table1*>table1;
	std::vector<Table39*> table39;
	std::vector<Table39*> table40;

	//citim datele din fisiere
	tabel1 = t.readTable1(t1);
	tabel39 = t.readTable39(t39);
	tabel40 = t.readTable39(t40);
}

//Taskuri::Taskuri(std::ifstream ftabel1, std::ifstream ftabel39, std::ifstream ftabel40)
//{
//
//	util util;
//	tabel1 = util.readTable1(ftabel1);
//	tabel39 = util.readTable39(ftabel39);
//	tabel40 = util.readTable39(ftabel40);
//}



Taskuri::~Taskuri()
{
}
