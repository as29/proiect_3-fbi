#include <stdlib.h>
#include "Taskuri.h"
#include "Task2.h"

Task2::Task2() : Response("Task2")
{

}

std::string Task2::interpretPacket(const boost::property_tree::ptree& packet)
{
	auto an1 = atoi(packet.get<std::string>("An1").c_str());
	auto an2 = atoi(packet.get<std::string>("An2").c_str());

	float furturi=0;
	//float furturi = Util.fct1()
	Taskuri task;

	furturi = task.sumaFurturi(an1, an2);

	this->content.push_back(boost::property_tree::ptree::value_type("File", "procent.txt"));
	this->content.push_back(boost::property_tree::ptree::value_type("furturi", std::to_string(furturi)));

	return this->getContentAsString();
}



