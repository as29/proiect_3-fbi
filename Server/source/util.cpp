#pragma once
#include "util.h"
#include <string>
#include <stdlib.h>
#include<iostream>
std::vector<Table1*> util::readTable1(std::ifstream & file)
{
	std::vector<Table1*> list;
	int colIdx = 0;
	//int rowIdx = 0;
	std::string line;
	Table1 *row = new Table1();
	// adauga in lista primul rand
	list.push_back(row);
	int contor = 0;
	while (std::getline(file, line)) {
		colIdx++;
		//contor++;
		//if (contor == 891) {
		//	int aaa = 0;
		//}

		if (colIdx % 20 == 0) {
			//std::cout << colIdx << std::endl;
			// rowIdx++;	
			row = new Table1();
			list.push_back(row);
		}
		int year;
		double d;
		float f;
		// setez fiecare atribut la pasul corespunzator
		switch (colIdx % 20) {
		case 1:
			year = std::stoi(line);
			row->setYear(year);
			break;
		case 2:
			// line to double
			d = atof(line.c_str());
			row->setPopulation(d);
			break;
		case 3:
			d = atof(line.c_str());
			row->setViolentCrime(d);
			break;
		case 4:
			f = atof(line.c_str());
			row->setViolentCrime_r(f);
			break;
		case 5:
			d = atof(line.c_str());
			row->setMurderNM(d);
			break;
		case 6:
			f = atof(line.c_str());
			row->setMurderNM_r(f);
			break;
		case 7:
			d = atof(line.c_str());
			row->setRape(d);
			break;
		case 8:
			f = atof(line.c_str());
			row->setRape_r(f);
			break;
		case 9:
			d = atof(line.c_str());
			row->setRobbery(d);
			break;
		case 10:
			f = atof(line.c_str());
			row->setRobbery_r(f);
			break;
		case 11:
			d = atof(line.c_str());
			row->setAggravatedAssault(d);
			break;
		case 12:
			f = atof(line.c_str());
			row->setAggravatedAssault_r(f);
			break;
		case 13:
			d = atof(line.c_str());
			row->setPropertyCrime(d);
			break;
		case 14:
			f= atof(line.c_str());
			row->setPropertyCrime_r(f);
			break;
		case 15:
			d = atof(line.c_str());
			row->setBurglary(d);
			break;
		case 16:
			f = atof(line.c_str());
			row->setBurglary_r(f);
			break;
		case 17:
			d = atof(line.c_str());
			row->setLarencyTeft(d);
			break;
		case 18:
			f = atof(line.c_str());
			row->setLarencyTeft_r(f);
			break;
		case 19:
			d = atof(line.c_str());
			row->setMotorVehicleTeft(d);
			break;
		case 20:
			d = atof(line.c_str());
			row->setMotorVehicleTeft_r(d);
			break;
		}
	}
	list.pop_back();
	return list;
}

std::vector<Table39*> util::readTable39(std::ifstream & f)
{
	std::string line;

	std::vector<Table39*> list;

	int colIdx = 0;
	//int rowIdx = 0;
	Table39 *row = new Table39();
	// adauga in lista primul rand
	list.push_back(row);
	int contor = 0;
	while (std::getline(f, line)) {
		colIdx++;
		contor++;
		if (contor == 891) {
			int aaa = 0;
		}

		// creez un obiect odata la 28 de pasi (coloane)
		if (colIdx % 27 == 0) {
			//std::cout << colIdx << std::endl;
			// rowIdx++;	
			row = new Table39();
			list.push_back(row);
		}

		double d;
		// setez fiecare atribut la pasul corespunzator
		switch (colIdx % 27) {
		case 1:
			row->setOffenseCharged(line);
			break;
		case 2:
			// line to double
			d = atof(line.c_str());
			row->setTotalAllAges(d);
			break;
		case 3:
			d = atof(line.c_str());
			row->setAgesUnder15(d);
			break;
		case 4:
			d = atof(line.c_str());
			row->setAgesUnder18(d);
			break;
		case 5:
			d = atof(line.c_str());
			row->setAgesOver18(d);
			break;
		case 6:
			d = atof(line.c_str());
			row->setAgesUnder10(d);
			break;
		case 7:
			d = atof(line.c_str());
			row->setAges10_12(d);
			break;
		case 8:
			d = atof(line.c_str());
			row->setAges13_14(d);
			break;
		case 9:
			d = atof(line.c_str());
			row->setAge15(d);
			break;
		case 10:
			d = atof(line.c_str());
			row->setAge16(d);
			break;
		case 11:
			d = atof(line.c_str());
			row->setAge17(d);
			break;
		case 12:
			d = atof(line.c_str());
			row->setAge18(d);
			break;
		case 13:
			d = atof(line.c_str());
			row->setAge19(d);
			break;
		case 14:
			d = atof(line.c_str());
			row->setAge20(d);
			break;
		case 15:
			d = atof(line.c_str());
			row->setAge21(d);
			break;
		case 16:
			d = atof(line.c_str());
			row->setAge22(d);
			break;
		case 17:
			d = atof(line.c_str());
			row->setAge23(d);
			break;
		case 18:
			d = atof(line.c_str());
			row->setAge24(d);
			break;
		case 19:
			d = atof(line.c_str());
			row->setAges25_29(d);
			break;
		case 20:
			d = atof(line.c_str());
			row->setAges30_34(d);
			break;
		case 21:
			d = atof(line.c_str());
			row->setAges35_39(d);
			break;
		case 22:
			d = atof(line.c_str());
			row->setAges40_44(d);
			break;
		case 23:
			d = atof(line.c_str());
			row->setAges45_49(d);
			break;
		case 24:
			d = atof(line.c_str());
			row->setAges50_54(d);
			break;
		case 25:
			d = atof(line.c_str());
			row->setAges55_59(d);
			break;
		case 26:
			d = atof(line.c_str());
			row->setAges60_64(d);
			break;
		case 27:
			d = atof(line.c_str());
			row->setAgesOver65(d);
			break;
		}
	}
	list.pop_back();
	return list;
}

util::util()
{
}


util::~util()
{
}
