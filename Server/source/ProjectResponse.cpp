#include <stdlib.h>

#include "ProjectResponse.h"

ProjectResponse::ProjectResponse() : Response("Project")
{

}

std::string ProjectResponse::interpretPacket(const boost::property_tree::ptree& packet)
{
	auto test1 = atoi(packet.get<std::string>("test1").c_str());
	auto test2 = atoi(packet.get<std::string>("test2").c_str());

	this->content.push_back(boost::property_tree::ptree::value_type("File", "project.txt"));
	this->content.push_back(boost::property_tree::ptree::value_type("Result", "informatia a ajuns"));

	return this->getContentAsString();
}
