#pragma once

#include "Response.h"

class Task5 : public Framework::Response
{
public:
	Task5();

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};