#pragma once
#include<iostream>
#include <string>
class Table39
{
public:
	Table39();
	~Table39();

	Table39(std::string offenseCharged, double totalAllAges, double agesUnder15, double agesUnder18, double agesOver18, double under10, double age10_12, double age13_14, double age15, double age16, double age17, double age18, double age19, double age20, double age21
		, double age22, double age23, double age24, double age25_29, double age30_34, double age35_39, double age40_44, double age45_49, double age50_54, double age55_59, double age60_64, double ageOver65);
public:
	std::string offenseCharged;
	double totalAllAges;
	double agesUnder15;
	double agesUnder18;
	double agesOver18;
	double under10;
	double age10_12;
	double age13_14;
	double age15;
	double age16;
	double age17;
	double age18;
	double age19;
	double age20;
	double age21;
	double age22;
	double age23;
	double age24;
	double age25_29;
	double age30_34;
	double age35_39;
	double age40_44;
	double age45_49;
	double age50_54;
	double age55_59;
	double age60_64;
	double ageOver65;

	std::string getOffenseCharged();
	double getTotalAllAges();
	double getTotalAllgetgetAgess();
	double getAgessUnder15();
	double getAgesUnder18();
	double getAgessOver18();
	double getAgessunder10();
	double getAges10_12();
	double getAges13_14();
	double getAge15();
	double getAge16();
	double getAge17();
	double getAge18();
	double getAge19();
	double getAge20();
	double getAge21();
	double getAge22();
	double getAge23();
	double getAge24();
	double getAges25_29();
	double getAges30_34();
	double getAges35_39();
	double getAges40_44();
	double getAges45_49();
	double getAges50_54();
	double getAges55_59();
	double getAges60_64();
	double getAgesOver65();


	void setOffenseCharged(std::string x);
	void setTotalAllAges(double x);
	void setAgesUnder15(double x);
	void setAgesUnder18(double x);
	void setAgesOver18(double x);
	void setAgesUnder10(double x);
	void setAges10_12(double x);
	void setAges13_14(double x);
	void setAge15(double x);
	void setAge16(double x);
	void setAge17(double x);
	void setAge18(double x);
	void setAge19(double x);
	void setAge20(double x);
	void setAge21(double x);
	void setAge22(double x);
	void setAge23(double x);
	void setAge24(double x);
	void setAges25_29(double x);
	void setAges30_34(double x);
	void setAges35_39(double x);
	void setAges40_44(double x);
	void setAges45_49(double x);
	void setAges50_54(double x);
	void setAges55_59(double x);
	void setAges60_64(double x);
	void setAgesOver65(double x);

	void print() {
		std::cout << "offenseCharged:" << offenseCharged << ",totalAllAges " << " agesUnder15 " << agesUnder15 << " agesUnder18 " << agesUnder18 << " agesOver18 " << agesOver18 << " under10 " << under10
			<< " age10_12 " << age10_12 << " age13_14 " << age13_14 << " age15 " << age15 << " age16 " << age16 << " age17 " << age17 << " age18 " << age18 << " age19 " << age19 << " age20 " << age20
			<< " age21 " << age21 << " age22 " << age22 << " age23 " << age23 << " age24 " << age24 << " age25_29 " << age25_29 << " age30_34 " << age30_34
			<< " age35_39 " << age35_39 << " age40_44 " << age40_44 << " age45_49 " << age45_49 << " age50_54 " << age50_54 << " age55_59 " << age55_59 << " age60_64 " << age60_64 << " ageOver65 " << ageOver65 << std::endl;
	}

}; 