#pragma once

#include "Response.h"

class ProjectResponse : public Framework::Response
{
public:
	ProjectResponse();

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};