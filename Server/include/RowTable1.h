#pragma once
#include <vector>
#include<fstream>
class RowTable1
{

public:
	RowTable1();
	RowTable1(int year,
		double population,
		double violentCrime,
		float violentCrime_r,
		double murderNM,
		float murderNM_r,
		double rape,
		float rape_r,
		double robbery,
		float robbery_r,
		double aggravatedAssault,
		float aggravatedAssault_r,
		double propertyCrime,
		float propertyCrime_r,
		double burglary,
		float burglary_r,
		double larencyTeft,
		float larencyTeft_r,
		double motorVehicleTeft,
		float motorVehicleTeft_r);
	~RowTable1();

public:
	int year;
	double population;
	double violentCrime;
	float violentCrime_r;
	double murderNM;
	float murderNM_r;
	double rape;
	float rape_r;
	double robbery;
	float robbery_r;
	double aggravatedAssault;
	float aggravatedAssault_r;
	double propertyCrime;
	float propertyCrime_r;
	double burglary;
	float burglary_r;
	double larencyTeft;
	float larencyTeft_r;
	double motorVehicleTeft;
	float motorVehicleTeft_r;

	int getYear();
	double getPopulation();
	double getViolentCrime();
	float getViolentCrime_r();
	double getMurderNM();
	float getMurderNM_r();
	double getRape();
	float getRape_r();
	double getRobbery();
	float getRobbery_r();
	double getAggravatedAssault();
	float getAggravatedAssault_r();
	double getPropertyCrime();
	float getPropertyCrime_r();
	double getBurglary();
	float getBurglary_r();
	double getLarencyTeft();
	float getLarencyTeft_r();
	double getMotorVehicleTeft();
	float getMotorVehicleTeft_r();

	void setYear(int);
	void setPopulation(double);
	void setViolentCrime(double);
	void setViolentCrime_r(float);
	void setMurderNM(double);
	void setMurderNM_r(float);
	void setRape(double);
	void setRape_r(float);
	void setRobbery(double);
	void setRobbery_r(float);
	void setAggravatedAssault(double);
	void setAggravatedAssault_r(float);
	void setPropertyCrime(double);
	void setPropertyCrime_r(float);
	void setBurglary(double);
	void setBurglary_r(float);
	void setLarencyTeft(double);
	void setLarencyTeft_r(float);
	void setMotorVehicleTeft(double);
	void setMotorVehicleTeft_r(float);
};
