#pragma once
#include <vector>
class Table1
{
public:
	Table1();
	~Table1();

public:
	int year;
	double population;
	double violentCrime;
	float violentCrime_r;
	double murderNM;
	float murderNM_r;
	double rape;
	float rape_r;
	double robbery;
	float robbery_r;
	double aggravatedAssault;
	float aggravatedAssault_r;
	double propertyCrime;
	float propertyCrime_r;
	double burglary;
	float burglary_r;
	double larencyTeft;
	float larencyTeft_r;
	double motorVehicleTeft;
	float motorVehicleTeft_r;
	

	int getYear()
	{
		return year;
	}
	double getPopulation()
	{
		return population;
	}
	double getViolentCrime()
	{
		return violentCrime;
	}

	float getViolentCrime_r() {
		return violentCrime_r;
	}
	double GetMurderNM() {
		return murderNM;
	}
	float getMurderNM_r() {
		return murderNM_r;
	}
	double getRape() {
		return rape;
	}
	float getRape_r() {
		return rape_r;
	}

	double getRobbery() {
		return robbery;
	}

	float getRobbery_r() {
		return robbery_r;
	}
	double getAggravatedAssault() {
		return aggravatedAssault;
	}
	float getAggravatedAssault_r() {
		return aggravatedAssault_r;
	}
	double getPropertyCrime() {
		return propertyCrime;
	}
	float getPropertyCrime_r() {
		return propertyCrime_r;
	}
	double getBurglary() {
		return burglary;
	}
	float getBurglary_r() {
		return burglary_r;
	}
	double getLarencyTeft() {
		return larencyTeft;
	}
	float getLarencyTeft_r() {
		return larencyTeft_r;
	}
	double getMotorVehicleTeft() {
		return motorVehicleTeft;
	}
	float getMotorVehicleTeft_r() {
		return motorVehicleTeft_r;
	}

	//set
	void setYear(int value)
	{
		year = value;
	}
	void setPopulation(double value)
	{
		population = value;
	}
	void setViolentCrime(double value)
	{
		violentCrime = value;
	}

	void setViolentCrime_r(float value)
	{
		violentCrime_r = value;
	}
	void setMurderNM(double value)
	{
		murderNM = value;
	}
	void setMurderNM_r(float value)
	{
		murderNM_r = value;
	}
	void setRape(double value)
	{
		rape = value;
	}
	void setRape_r(float value)
	{
		rape_r = value;
	}

	void setRobbery(double value)
	{
		robbery = value;
	}

	void setRobbery_r(float value)
	{
		robbery_r = value;
	}
	void setAggravatedAssault(double value)
	{
		aggravatedAssault = value;
	}
	void setAggravatedAssault_r(float value)
	{
		aggravatedAssault_r = value;
	}
	void setPropertyCrime(double value)
	{
		propertyCrime = value;
	}
	void setPropertyCrime_r(float value)
	{
		propertyCrime_r = value;
	}
	void setBurglary(double value)
	{
		burglary = value;
	}
	void setBurglary_r(float value) {
		burglary_r = value;
	}
	void setLarencyTeft(double value) {
		larencyTeft = value;
	}
	void setLarencyTeft_r(float value) {
		larencyTeft_r = value;
	}
	void setMotorVehicleTeft(double value) {
		motorVehicleTeft = value;
	}
	void setMotorVehicleTeft_r(float value) {
		motorVehicleTeft_r = value;
	}

};
