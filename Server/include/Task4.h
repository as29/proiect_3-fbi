#pragma once

#include "Response.h"

class Task4 : public Framework::Response
{
public:
	Task4();

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};