#pragma once

#include "Response.h"

class Task2 : public Framework::Response
{
public:
	Task2();

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};