#pragma once

#include "Response.h"

class Task3 : public Framework::Response
{
public:
	Task3();

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};