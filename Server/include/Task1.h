#pragma once

#include "Response.h"

class Task1 : public Framework::Response
{
public:
	Task1();

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};